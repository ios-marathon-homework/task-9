//
//  ViewController2.swift
//  Task 9
//
//  Created by Владислав Положай on 4/8/22.
//

import UIKit

class ViewController2: UIViewController {

    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backButton(_ backButton: UIButton) {
        dismiss(animated: true)
    }

    deinit {
     print("Deallocated")
    }
}
